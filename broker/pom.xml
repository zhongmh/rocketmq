<!-- Licensed to the Apache Software Foundation (ASF) under one or more contributor
	license agreements. See the NOTICE file distributed with this work for additional
	information regarding copyright ownership. The ASF licenses this file to
	You under the Apache License, Version 2.0 (the "License"); you may not use
	this file except in compliance with the License. You may obtain a copy of
	the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required
	by applicable law or agreed to in writing, software distributed under the
	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
	OF ANY KIND, either express or implied. See the License for the specific
	language governing permissions and limitations under the License. -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <groupId>org.apache.rocketmq</groupId>
        <artifactId>rocketmq-all</artifactId>
        <version>5.1.1-SNAPSHOT</version>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <packaging>jar</packaging>
    <artifactId>rocketmq-broker</artifactId>
    <name>rocketmq-broker ${project.version}</name>

    <properties>
        <project.root>${basedir}/..</project.root>
    </properties>

    <dependencies>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>rocketmq-remoting</artifactId>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>rocketmq-store</artifactId>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>rocketmq-tiered-store</artifactId>
        </dependency>
        <dependency>
            <groupId>io.github.aliyunmq</groupId>
            <artifactId>rocketmq-slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>io.github.aliyunmq</groupId>
            <artifactId>rocketmq-logback-classic</artifactId>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>rocketmq-client</artifactId>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>rocketmq-srvutil</artifactId>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>rocketmq-filter</artifactId>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>rocketmq-acl</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
        </dependency>
        <dependency>
            <groupId>org.javassist</groupId>
            <artifactId>javassist</artifactId>
        </dependency>
        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcpkix-jdk15on</artifactId>
        </dependency>
        <dependency>
            <groupId>com.googlecode.concurrentlinkedhashmap</groupId>
            <artifactId>concurrentlinkedhashmap-lru</artifactId>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin.version}</version>
                <configuration>
                    <forkCount>1</forkCount>
                    <reuseForks>false</reuseForks>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy</id>
                        <phase>package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/lib</outputDirectory>
                            <overWriteReleases>false</overWriteReleases>
                            <overWriteSnapshots>false</overWriteSnapshots>
                            <overWriteIfNewer>true</overWriteIfNewer>
                            <useRepositoryLayout>true</useRepositoryLayout>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <classpathPrefix>lib</classpathPrefix>
                            <mainClass>org.apache.rocketmq.broker.BrokerStartup</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>

            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <version>1.2.2</version>
                <executions>
                    <execution>
                        <id>build-image</id>
                        <phase>package</phase>
                        <goals>
                            <goal>build</goal>
                        </goals>
                        <configuration>
                            <useGitCommitId>true</useGitCommitId>
                            <baseImage>java:openjdk-8-jre</baseImage>
                            <imageName>rocketmq-broker</imageName>
                            <user>rocketmq</user>
                            <workdir>/home/rocketmq</workdir>
                            <env>
                                <ROCKETMQ_HOME>/home/rocketmq</ROCKETMQ_HOME>
                            </env>
                            <exposes>
                                <expose>6888</expose>
                            </exposes>
                            <runs>useradd rocketmq &amp;&amp; mkdir logs</runs>
                            <cmd>java -Xmx256m -Xms128m -jar /home/rocketmq/rocketmq-broker-5.1.1-SNAPSHOT.jar -c ~/conf/broker.conf</cmd>
                            <imageTags>
                                <imageTag>latest</imageTag>
                            </imageTags>
                            <resources>
                                <resource>
                                    <targetPath>/home/rocketmq</targetPath>
                                    <directory>${project.build.directory}</directory>
                                    <include>*.jar</include>
                                </resource>
                                <resource>
                                    <targetPath>/home/rocketmq/lib</targetPath>
                                    <directory>${project.build.directory}/lib</directory>
                                </resource>
                                <resource>
                                    <targetPath>/home/rocketmq/conf</targetPath>
                                    <directory>${project.build.directory}/classes</directory>
                                    <include>broker.conf</include>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
